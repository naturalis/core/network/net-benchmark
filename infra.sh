#!/bin/bash



. .env

function deploy_infra(){

    for host in $hosts

    do
    	ssh ubuntu@$host sudo apt update
    	ssh ubuntu@$host sudo apt install -y jq jc traceroute dnsutils
	ssh ubuntu@$host rm -rf  /home/ubuntu/netbenchmark
	ssh ubuntu@$host mkdir -p /home/ubuntu/net-benchmark

	rsync -larv scripts ubuntu@$host:/home/ubuntu/net-benchmark/
	rsync -larv go/httpd/server ubuntu@$host:/home/ubuntu/net-benchmark/


    done

}


#deploy_infra

function start_infra(){
    echo "$hosts \n \n"

    for host in $hosts

    do
    	ssh ubuntu@$host screen -d -m /home/ubuntu/net-benchmark/server 

    done

}


#start_infra


function infra_deploy(){
    echo "$hosts \n \n"

    for host in $hosts

    do
    	ssh ubuntu@$host sudo apt update
    	ssh ubuntu@$host sudo apt install jq jc traceroute dnsutils
	ssh ubuntu@$host mkdir -p /home/ubuntu/netbenchmark
	rsync -larv scripts ubuntu@$host:/home/ubuntu/netbenchmark/
	rsync -larv go/httpd/server ubuntu@$host:/home/ubuntu/netbenchmark/


    done

}

function ping_host(){
	PING_DATA=$(curl -s http://$host:8042/ping)
	PING="- ping: $PING_DATA"
}	

function dns(){
        DNS=$(curl -s http://$host:8042/dns)
        }


function tcpstream(){
	# TCP_DATA=$(curl -o /dev/null -w '"%{http_code} %{time_total}' -s http://$host:8042/tcpstream' | sed -e s'/s//')
	TCP_DATA=$(curl -s http://$host:8042/tcpstream)
	TCP="-tcpstr: $TCP_DATA"
 }


function test_infra(){
for host in $hosts

do
	tcpstream
	ping_host
	dns
	echo $host $PING $TCP $DNS

done
}

case $1 in
  deploy)
	  deploy_infra
	  ;;
  start)
          start_infra
	  ;;
  test)
	  echo -e "$hosts \n\n\n"
          test_infra
	  ;;
   testloop)
	  echo -e "$hosts \n\n\n"
	  while [ true ]
	  do 
          	test_infra
  	  done
          ;;
  *)
     echo "usage deploy , start, or test or testloop" 
     ;;
esac

exit 0
