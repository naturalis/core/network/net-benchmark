# net-benchmark

```
mkdir gitlab
cd gitlib
clone thins repo
```

create .env with hosts like:

```
hosts="10.10.1.4 10.10.1.8 10.10.1.1 10.10.1.10 10.10.1.11"

```

This environment assumes ununtu 22.4 with ubuntu user with sudo, homedir /home/ubuntu and /home/ubuntu/net-brench as free path to copy files 
such hard coded



Setup :
Distributed network test framework to test from different places in the local network and remote.

```
./infra.sh deploy
./infra.sh start
./infra.sh test
./infra.sh testloop
```

example:

```mermaid
flowchart TD
    A[Engineer Notebook]
    B[WireguardServer]
    C(Remote Testnode)
    D(Office lan testnode)
    E(datacenter Baremetal testnode)
    F(datacenter VMtestnode)
    
    I(((Internet)))
    BR(((BorderRouter)))
    FWO(((Outer firewall)))
    FWI(((Inner Firewall)))
    IR(((InnnerRouter)))
    CR(((Campus Router)))
    DCR(((Datacenter Router)))


subgraph Net [Network]
    I === BR
    BR === FWO
    FWO === IR
    IR === DCR
    IR === FWI
    FWI === CR
    end

    C === I
    D === CR
    E === DCR
    F === DCR

    

    B --- A
    C --- B
    D --- B
    E --- B
    F --- B
  
   
````


### Files
```
├── blobs
│   └── 1m.blob ==> 1 Mb test down load
├── go
│   └── httpd
│       ├── go.mod
│       ├── go.sum
│       ├── server ==> webserver
│       └── server.go
├── infra.sh ==> script to deploy,start,test and test in a loop 
├── LICENSE
├── README.md
└── scripts  ==> test scrips
    ├── ping_json.sh
    ├── portscan_json.py
    ├── tcp_small_stream.sh
    ├── traceroute_json.sh
    └── udp_dns.sh
````

 

###Tools: 
- bash scripts
- ping
- traceroute 
- go_lang [speedtest](https://github.com/librespeed/speedtest) 
- go_lang [http_server](https://blog.logrocket.com/creating-a-web-server-with-golang/) 
- wireguard


### Conky

This provides ad standalone graph of the testing infra.

in ubuntu:
```
sudo apt install conky-all
./mk_conky_cfg.sh >   my_config
conky -c my_config
```


### Todos:
- scripts need to be linted
- librespeedtest needs to be implemented, or in go_lang and/or in shell script  and a server.json
- there still some scripts outside this repo in /home/ubuntu , those need to sanitaized and added (or removed from go/httpd/server.go
- automated wireguard (currently manual
- fix hard coded paths
- implement iperf3

