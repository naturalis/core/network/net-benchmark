#!/bin/bash


. .env

echo '
conky.config = {
    alignment = '\''bottom_left'\'',
    background = false,
    border_width = 1,
    cpu_avg_samples = 2,
    default_color = '\''white'\'',
    default_outline_color = '\''white'\'',
    default_shade_color = '\'white\'',
    double_buffer = true,
    draw_borders = false,
    draw_graph_borders = false,
    draw_outline = false,
    draw_shades = false,
    extra_newline = false,
    font = '\''DejaVu Sans Mono:size=12'\'',
    gap_x = 160,
    gap_y = 60,
    minimum_height = 5,
    minimum_width = 5,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
    own_window = true,
    own_window_class = '\''Conky'\'',
    own_window_type = '\''normal'\'',
    show_graph_range = false,
    show_graph_scale = true,
    stippled_borders = 0,
    update_interval = 1.0,
    uppercase = false,
    use_spacer = '\''none'\'',
    use_xft = true,
    xinerama_head = 1,
}

conky.text = [[
--------------------------------------------------------------------------------------------------------------

'

for host in $hosts

do
#	echo $host

	#echo "--- Node ${host}"
        echo '---- Node '${host}' ----  ping_time ${execp "ping '$host' -c 1 | jc --ping | jq .packets_received"} '\
		'${execp "ping '$host' -c 1 | jc --ping | jq .responses[].time_ms"} 
		${execigraph 1 "ping -c 1 '$host' |  jc --ping | jq .responses[].time_ms" }'
        echo 'ping 1.1.1.1  ${execp "curl -s http://'$host':8042/ping | awk '\''{print $2 }'\''"}
		${execigraph 1 "curl -s http://'$host':8042/ping | awk '\''{print $3 }'\''"}'
        echo 'dns time ${execp "curl -s http://'$host':8042/dns | awk '\''{print $6 }'\''"} 
                ${execigraph 1  "curl -s http://'$host':8042/dns | awk '\''{ print $10 }'\''"}'
        echo 'tcp time 1Mb ${execp "curl -s http://'$host':8042/tcpstream | awk '\''{print $2 }'\''"}
                ${execigraph 1 "curl -s http://'$host':8042/tcpstream | awk '\''{ print $3 }'\''"}'


#	echo "$host "'${execp "ping '$host' -c 1 | jc --ping | jq .packets_received"} ${execigraph  1 "ping -c 1 '$host'| jc --ping | jq .responses[].time_ms" }'
#       echo 'ping 1.1.1.1  ${execp "curl -s http://'$host':8042/ping | awk '\''{print $1 }'\''"}' 
#	echo 'dns time ${execigraph  1  "curl -s http://'${host}':8042/dns | awk '\''{ print $6 }'\''"}'
#	echo  'tcpstream ${execigraph  1 "curl -s http://'${host}':8042/tcpstream | awk '\''{ print $2 }'\''"} '


done
echo ']]' 

exit 0

#        echo 'ping  ${execp "curl -s http://'$host':8042/ping | cut -d " " -f 1" }'      
#	echo 'dns time ${execigraph  1  \"curl -s http://'${host}':8042/dns | awk \'{ print $6 }\'"} '
#	tcpstream ${execigraph  1 "curl -s http://'${host}':8042/tcpstream | awk \'{ print $2 }\'"} '
#	time \${execigraph  1  "curl -s http://'${host}':8042/ping | awk \'{print $2 }\'"}'
