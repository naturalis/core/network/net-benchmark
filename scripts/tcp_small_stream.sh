#!/bin/bash



## used to detect issues tcp where session worked, however this cmd was used to see
# if small stream gives issues (network issue Writing a config to ASIC worked not ok)


url='https://gitlab.com/naturalis/core/network/net-benchmark/-/raw/main/blobs/1m.blob'



DATA=$(curl -o /dev/null -w "%{http_code} %{time_total}" -s $url)
HTTP_CODE=$(echo $DATA | cut -d ' ' -f1)
REPONSE_TIME=$(echo $DATA | cut -d ' ' -f2)

minimun=$(echo $REPONSE_TIME | cut -d '.' -f 1) 

if [  "$minimun"  -lt 3 ]
then
	conky_time=3
else
	conky_time=$REPONSE_TIME
fi 

echo $HTTP_CODE $REPONSE_TIME $conky_time

exit 0

