
import nmap
import json

nm = nmap.PortScanner()
nm.scan('10.160.1.3', '0-10024')

#print  nm.command_line()

#print (' %s ' % (nm.scaninfo())


for host in nm.all_hosts():
  for proto in nm[host].all_protocols():
    lport = nm[host][proto].keys()

    myports = [ ]  
    for port in lport:
     # print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))
      myports.append(port)
  
  scan_dict = { "host": host, "source": "remote",  "ports":  myports }

  # convert into JSON:
  scan_json = json.dumps(scan_dict)

  # the result is a JSON string:
  print (scan_json)

