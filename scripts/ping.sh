#!/bin/bash

DATA=$(ping -c 1 1.1.1.1 | jc --ping)

RESP=$(echo $DATA | jq .packets_received)
MS_TIME=$(echo $DATA | jq '.responses[].time_ms')

minimun=$(echo $MS_TIME | cut -d '.' -f 1) 

if [  "$minimun"  -lt 3 ]
then
	conky_time=3
else
	conky_time=$MS_TIME
fi 

echo $RESP $MS_TIME $conky_time

