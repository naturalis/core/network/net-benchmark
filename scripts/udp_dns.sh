#!/bin/bash


i=0

# get local dns
dns=$(resolvectl | grep 'Current DNS Server' | head -n 1 | awk '{print $4 }')

## or use other
#dns=1.1.1.1

domain=example.org


## jq examples
# dig example.com | jc --dig | jq -r '.[].answer[].data'
# dig example.com | jc --dig | jq '.[].query_time'

dns_data=$(dig $domain @$dns | jc --dig)

MSTIME=$(echo $dns_data | jq -r '.[].query_time')
RESPONSE=$(echo $dns_data | jq -r '.[].answer[].data')

minimun=$(echo $MSTIME | cut -d '.' -f 1)

if [ "$minimun" -lt "3" ]
then
	conky_time="3"
else
	conky_time=$RESPONSE
fi

echo  domian: $domain dnsserver: $dns milisec: $MSTIME ipadres: $RESPONSE conky_time: $conky_time





