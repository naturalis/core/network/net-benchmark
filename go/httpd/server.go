package main 

import (
	"os"
	"os/exec"
	"errors"
	"io"
	"net/http"
        "fmt"
     //   "net"
     //   "time"
     //   goping "github.com/digineo/go-ping"
)

func getRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got / request\n")
	io.WriteString(w, "Try like:\n/is_het_netwerk_ok\n/ping\n/internetspeedtest\n/traceoute\n/hello\n/netdcspeedtest\n/netdw2speedtest\n/ping_vlans\n/dhcp\n")

}
 
/*

func getGoPing(w http.ResponseWriter, r *http.Request) {
        fmt.Printf("got /goping request\n")
//	output, _ := goping()
        goping()
	fmt.Println(string(ping_msg))
        io.WriteString(w, string(ping_msg))
 //       io.WriteString(w, "goping 1.1.1.1 ok\n")
}

*/

func getOK(w http.ResponseWriter, r *http.Request) {
        fmt.Printf("got /is_het_netwerk_ok request\n")
        io.WriteString(w, "ja!\n")
}


func getHello(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got /hello request\n")
	io.WriteString(w, "Hello Allemaal!\n")
}




func getInternetSpeedtest(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("got /internetspeedtest request\n")
    cmd := exec.Command("/home/ubuntu/ams_pub_speedtest.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func getDHCP(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("got /dhcp request\n")
    cmd := exec.Command("/home/ubuntu/dhcp_leases_vlan_check.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func getPingVLANS(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("got /ping_vlans request\n")
    cmd := exec.Command("/home/ubuntu/ping_priv_vlans.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}


func getNetDcSpeedtest(w http.ResponseWriter, r *http.Request) {
    cmd := exec.Command("/home/ubuntu/netdc-speedtest.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func getNetDw2Speedtest(w http.ResponseWriter, r *http.Request) {
    cmd := exec.Command("/home/ubuntu/netdw2-speedtest.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func getping(w http.ResponseWriter, r *http.Request) {
    cmd := exec.Command("/home/ubuntu/net-benchmark/scripts/ping.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func getpingjson(w http.ResponseWriter, r *http.Request) {
    cmd := exec.Command("/home/ubuntu/ping_json.sh" )
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
     }

func gettraceroute(w http.ResponseWriter, r *http.Request) {
    cmd := exec.Command("traceroute", "1.1.1.1")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func gettraceroutejson(w http.ResponseWriter, r *http.Request) {
    cmd := exec.Command("/home/ubuntu/traceroute_json.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}
func getDNS(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("got /dns request\n")
    cmd := exec.Command("/home/ubuntu/net-benchmark/scripts/udp_dns.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}
func getTCPstream(w http.ResponseWriter, r *http.Request) {
    fmt.Printf("got /tcp_stream request\n")
    cmd := exec.Command("/home/ubuntu/net-benchmark/scripts/tcp_small_stream.sh")
    output, _ := cmd.CombinedOutput()
     fmt.Println(string(output))
     io.WriteString(w, string(output))
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", getRoot)
	mux.HandleFunc("/hello", getHello)
	mux.HandleFunc("/ping", getping)
	mux.HandleFunc("/ping_json", getpingjson)
	mux.HandleFunc("/is_het_netwerk_ok", getOK)
	mux.HandleFunc("/traceroute", gettraceroute)
	mux.HandleFunc("/traceroute_json", gettraceroutejson)
	mux.HandleFunc("/internetspeedtest", getInternetSpeedtest )
	mux.HandleFunc("/netdcspeedtest", getNetDcSpeedtest )
	mux.HandleFunc("/netdw2speedtest", getNetDw2Speedtest )
//	mux.HandleFunc("/goping", getGoPing )
	mux.HandleFunc("/dhcp", getDHCP )
	mux.HandleFunc("/dns", getDNS )
	mux.HandleFunc("/ping_vlans", getPingVLANS )
	mux.HandleFunc("/tcpstream", getTCPstream)
	
	err := http.ListenAndServe(":8042", mux)

	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error starting server: %s\n", err)
		os.Exit(1)
	}
}
