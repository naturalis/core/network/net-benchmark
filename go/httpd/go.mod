module stripe.dev/samples

go 1.18

require (
	github.com/librespeed/speedtest-cli v1.0.10
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
)

require (
	github.com/briandowns/spinner v1.12.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/digineo/go-logwrap v0.0.0-20181106161722-a178c58ea3f0 // indirect
	github.com/digineo/go-ping v1.0.1 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/go-ping/ping v0.0.0-20210407214646-e4e642a95741 // indirect
	github.com/gocarina/gocsv v0.0.0-20210408192840-02d7211d929d // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	golang.org/x/net v0.0.0-20210421230115-4e50805a0758 // indirect
	golang.org/x/sys v0.0.0-20210421221651-33663a62ff08 // indirect
)
